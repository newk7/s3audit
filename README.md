
This script will query all buckets in the AWS account for public access.

If the --test flag is set, then 5 buckets will be created with public access

If the --cleanup flag is set, then all of the test buckets will be deleted after the audit runs

Set both flags to create test buckets, run the audit, then clean up test buckets
