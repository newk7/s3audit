import boto3, json, argparse
from s3SetupPublic import delete_bucket, create_bucket

parser = argparse.ArgumentParser()
parser.add_argument('--test', action="store_true", help='set flag to generate test buckets with public access')
parser.add_argument('--cleanup', action="store_true", help='set flag for cleaning up demo buckets after run')
testing = parser.parse_args().test
cleaner = parser.parse_args().cleanup


def audit_s3():
    #set up AWS client
    client = boto3.client('s3')
    s3 = boto3.resource('s3')

    # open logging file
    f = open("AuditLog.txt", "w")

    #get a list of all the buckets in the AWS account
    response = client.list_buckets()

    #loop through all the buckets
    for bucket in response['Buckets']:
        bucket_name = bucket.get('Name')

        #get ACL for each bucket in the account
        bucket_acl = client.get_bucket_acl(Bucket=bucket_name)
        
        grants = bucket_acl.get('Grants')
        
        #check each grant for the acl for each bucket. if it has a URI of http://acs.amazonaws.com/groups/global/AllUsers , it has public access
        for grant in grants:
            if grant.get('Grantee').get('URI') is not None:
                if grant.get('Grantee').get('URI') == 'http://acs.amazonaws.com/groups/global/AllUsers':
                    f.write(bucket_name + ' has Public Access \n')
                    print(bucket_name,' has Public Access')
            else:
                pass
    
    f.close()
# create some public buckets for testing
if testing:
    for i in range(5):
        print(create_bucket('ian-public-audit'+str(i)))

#run the audit to check for pubblic access on buckets
audit_s3()

# clean up public test buckets
if cleaner:
    for i in range(5):
        print(delete_bucket('ian-public-audit'+str(i)))

