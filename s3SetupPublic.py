import boto3
from botocore.exceptions import ClientError

def create_bucket(bucket_name):
    """Create an S3 bucket in a specified region

    If a region is not specified, the bucket is created in the S3 default
    region (us-east-1).

    :param bucket_name: Bucket to create
    :param region: String region to create bucket in, e.g., 'us-west-2'
    :return: True if bucket created, else False
    """

    # Create bucket
    # try:
    s3_client = boto3.client('s3')
    location = {'LocationConstraint': 'us-east-2'}
    s3_client.create_bucket(Bucket=bucket_name, ACL='public-read-write', CreateBucketConfiguration=location)
    # except ClientError as e:
        # return False
    return bucket_name + " created"

def delete_bucket(bucket_name):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    bucket.delete(bucket_name)
    return bucket_name+ " bucket deleted"

# print(create_bucket('ian-boto-test'))
    # print(delete_bucket('ian-boto-test'))